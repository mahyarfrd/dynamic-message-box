let baseColor: string = (<any>window).messageConfig.baseColor || '#66b44e'

let style : string = `
    @keyframes showsendmessagebox {
        from {
            bottom: 0px;
            opacity: 0
        }
        to {
            bottom: 30px;
            opacity: 1
        }
    }
    @keyframes hidesendmessagebox {
        from {
            bottom: 20px;
            opacity: 1
        }
        to {
            bottom: 0;
            opacity: 0
        }
    }
    .message-element{
        direction: rtl;
        position: absolute;
        bottom: 30px; 
        right: 30px;
        border-radius: 50%;
        transition: all 0.5s
    }
    .message-element:hover{
        transform : scale(1.1)
    }
    .send-message-box-show{
        display:block;
        animation: showsendmessagebox 0.5s;
    }

    .send-message-box-hide{
        animation: hidesendmessagebox 0.5s;
        display:none;
    }
    .send-message-box#send-message-box{
        width:400px;
        height: 600px;
        max-width: 400px ;
        overflow: auto;
        position: absolute;
        right: 30px;
        bottom:30px;
        border-radius: 5px;
        background-color: #f8f8f8;
        box-shadow: 0 2px 5px #ccc;
    }
    #send-message-box::-webkit-scrollbar {
        width: 10px;
        background-color: transparent;
    }
    
    #send-message-box::-webkit-scrollbar-thumb {
      background-color: #ccc;
      border-radius: 5px
    }
    .send-message-box .message-header{
        width: 100%;
        padding: 20px 20px;
        border-radius: 5px 5px 0 0;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        box-sizing: border-box;
        direction: rtl;
    }
    .send-message-box .message-header img#close-message-box{
        width: 16px;
        height: 16px;
        margin-top: 3px;
    }
    .send-message-box .message-title{
        text-align: center;
    }
    .send-message-box #message-form{
        padding: 15px;
        direction: rtl;
        text-align: center;
    }
    .send-message-box #message-form .input-fields {
        width: 100%;
        background-color: transparent;
        border: none;
        border-bottom: 2px solid #cdcdcd;
        font-size: 16px;
        font-weight: bold;
        padding: 8px 5px;
        box-sizing: border-box;
        color: #434343;
        margin: 10px 0;
        transition: 0.5s all
    }
    .send-message-box #message-form .input-fields:focus{
        outline: unset;
        border-bottom: 2px solid ${baseColor};
    }
    .send-message-box #message-form .input-fields::placeholder{
        color:#a0a0a0;
        transition: 0.5s all
    }
    .send-message-box #message-form .input-fields:focus::placeholder{
        color: ${baseColor}
    }
    .send-message-box #message-form .textarea-fields {
        min-width: calc(100% - 5px);
        max-width: calc(100% - 5px);
        min-height: 60px;
        background-color: transparent;
        border: none;
        border-bottom: 2px solid #cdcdcd;
        font-size: 16px;
        font-weight: bold;
        padding: 8px 5px;
        box-sizing: border-box;
        color: #434343;
        margin: 10px 0;
        transition: 0.5s all;
    }
    .send-message-box #message-form .textarea-fields:focus{
        outline: unset;
        border-bottom: 2px solid ${baseColor};
    }
    .send-message-box #message-form .textarea-fields::placeholder{
        color:#a0a0a0;
        transition: 0.5s all
    }
    .send-message-box #message-form .textarea-fields:focus::placeholder{
        color: ${baseColor}
    }
    .send-message-box #message-form #submit-message{
        border: 2px solid ${baseColor};
        border-radius: 20px;
        margin: 10px auto;
        color: ${baseColor};
        background-color: transparent;
        padding: 10px 20px;
        font-size: 16px;
        transition: all .2s;
    }
    .send-message-box #message-form #submit-message:hover{
        background-color: ${baseColor};
        color: #fff;
    }
`

export default style
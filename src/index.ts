import style from './style'
import {messageButton} from './elements'

//add style of message box to html file
//start
let styleTag: HTMLStyleElement = document.createElement('style')

styleTag.innerHTML = style

document.head.appendChild(styleTag)
//end

//create message box and add it to html file
//start
let messageBox: HTMLDivElement = document.createElement("div")

let messageBoxId: Attr = document.createAttribute('id')

messageBoxId.value = "message-box" 

messageBox.setAttributeNode(messageBoxId)
//end

// add icon button to message box

messageBox.innerHTML = (messageButton)

document.body.appendChild(messageBox)

//add title and form to DOM

let titleBox: HTMLElement = document.getElementById('message-title')

if((<any>window).messageConfig.title){
    let titleTag: HTMLElement = document.createElement('h2')
    titleTag.style.color = (<any>window).messageConfig.title.color
    titleTag.innerHTML = (<any>window).messageConfig.title.value
    titleBox.appendChild(titleTag)
}
if((<any>window).messageConfig.subtitle){
    let subtitleTag:HTMLElement = document.createElement('h4')
    subtitleTag.style.color = (<any>window).messageConfig.subtitle.color
    subtitleTag.innerHTML = (<any>window).messageConfig.subtitle.value
    titleBox.appendChild(subtitleTag)
}

let formBox: HTMLElement = document.getElementById('message-form')

type inputfield = {name : string , type: string , lable: string}

if((<any>window).messageConfig.fields && (<any>window).messageConfig.fields !== []){ 
    let item :inputfield;
    for(item of (<any>window).messageConfig.fields){
        let input:HTMLInputElement = document.createElement('input')
        input.setAttribute('name' , item.name)
        input.setAttribute('id' , `ticket-${item.name}-id`)
        input.setAttribute('placeholder' , item.lable)
        input.setAttribute('type' , item.type)
        input.setAttribute('class' , 'input-fields')
        formBox.appendChild(input)
    }
    let textareaField: HTMLTextAreaElement = document.createElement('textarea')
    textareaField.setAttribute('class' , 'textarea-fields')
    textareaField.setAttribute('id' , 'ticket-body-id')
    textareaField.setAttribute('row' , '3')
    textareaField.setAttribute('name' , 'body')
    textareaField.setAttribute('placeholder' , 'پیام')
    formBox.appendChild(textareaField)
    let submitButton: HTMLButtonElement = document.createElement('button') 
    submitButton.setAttribute('id' , 'submit-message')
    submitButton.innerHTML = (<any>window).messageConfig.submitButtonTitle
    formBox.appendChild(submitButton)
}

//events

let messageButtonElement: HTMLElement = document.getElementById('message-button')

let sendMessageBox :HTMLElement = document.getElementById('send-message-box')

messageButtonElement.addEventListener('click' , ()=>{
    sendMessageBox.classList.remove('send-message-box-hide')
    sendMessageBox.classList.add("send-message-box-show")
})

let closeMessageButton = document.getElementById('close-message-box')

closeMessageButton.addEventListener('click' , ()=>{
    sendMessageBox.classList.remove("send-message-box-show")
    sendMessageBox.classList.add('send-message-box-hide')
})

let messageForm = document.getElementById('message-form')

//submit
messageForm.addEventListener('submit' , (e)=>{
    e.preventDefault()
    let item : inputfield;
    let formBody:{[key: string]: any} = {};
    for(item of (<any>window).messageConfig.fields){
        let value = (<HTMLInputElement>document.getElementById(`ticket-${item.name}-id`)).value
        formBody[item.name] = value
    }
    formBody['message'] = (<HTMLInputElement>document.getElementById(`ticket-body-id`)).value
    console.log(formBody)
})
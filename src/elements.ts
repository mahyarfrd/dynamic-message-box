import messageIcon from './assets/email.png'
import closeIcon from './assets/close.png'

let messageButton : string = `
    <div id='message-button' class='message-element'>
        <img src=${String(messageIcon)}>
    </div>
    <div id='send-message-box' class='send-message-box send-message-box-hide'>
        <div class='message-header'>
            <div></div>
            <img src=${String(messageIcon)}>
            <img id='close-message-box' src=${String(closeIcon)}>
        </div>
        <div id='message-title' class='message-title'>
            
        </div>
        <form id='message-form'>
    
        </form>
    </div>
`

export {
    messageButton
}